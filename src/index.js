import React, { Component } from 'react';
import ReactDOM from 'react-dom';

const Overview = ({agency : {cash, stocks}}) => {
  const total = cash +
                stocks.map(stock => stock.val).reduce((acc, nxt) => acc + nxt, 0);
  return (
    <div>Current value : {total}</div>
  )
}

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      client: [
        {
          name: 'Diptajit Basak',
          investments: [
            {
              amount: 1715.10,
              date: '2018-10-26T07:56:00.123Z'
            },
            {
              amount: 2106.80,
              date: '2018-11-05T07:56:00.123Z'
            }
          ]
        },
        {
          name: 'Sayan Naskar',
          investments: [
            {
              amount: 1671.60,
              date: '2018-10-30T07:56:00.123Z'
            }
          ]
        },
        {
          name: 'Saikat Karmakar',
          investments: [
            {
              amount: 2181.70,
              date: '2018-10-15T07:56:00.123Z'
            },
            {
              amount: 2315.80,
              date: '2018-10-28T07:56:00.123Z'
            }
          ]
        }
      ],
      agency: {
        cash: 1940.80,
        stocks: [
          {
            ticker: 'NSE: YESBANK',
            name: 'Yes Bank Limited',
            quantity: 20,
            val: 186.55
          },
          {
            ticker: 'NSE: HINDPETRO',
            name: 'Hindustan Petroleum Corporation Limited',
            quantity: 5,
            val: 249.80
          },
        ]
      }
    }
  }

  // {worthGraph}{stockPie}{clientDetails}
  render() {
    // console.log(this.state.agency.cash);
    return(
      <Overview agency={this.state.agency}/>
    );
  }
}


ReactDOM.render(<App/>, document.querySelector('.container'));